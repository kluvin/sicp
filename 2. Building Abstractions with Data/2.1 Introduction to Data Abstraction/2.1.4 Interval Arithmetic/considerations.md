* Exercise 2.7
 (define (make-interval a b) (cons a b))
 Define selectors upper-bound and lower-bound to complete the implementation.
  * Should the implementation care about ordering of the upper- & lower-bound, or just let them be a, b?
  