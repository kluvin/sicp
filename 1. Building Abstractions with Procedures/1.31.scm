#lang sicp

; product summation - both recursive and iterative
;   returns product of values of function over a given range
;   write factorial in terms of this
;   use product to approximate pi / 4

; Operator rewrite of original sum
; Last term is 1, not 0
(define (p-sum term a next b)
  (if (> a b)
      1
      (* (term a)
         (p-sum term (next a) next b))))

(define (p-sum-iter term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (* (term a) result))))
  (iter a 1))

(define (factorial n)
  (p-sum-iter
     (lambda (x) x)
     1
     (lambda (x) (+ x 1))
     n))

(define (pi-approx n)
  (p-sum-iter
   (lambda (x) (/ (nom x) (den x)))
   2
   (lambda (x) (+ x 1))
   n))

(define (nom n)
  (+ n (remainder n 2)))

(define (den n)
  (+ n (remainder (+ n 1) 2)))

(factorial 10)

;(pi-approx 200)