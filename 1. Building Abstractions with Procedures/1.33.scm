#lang sicp

; filter
;   accumulate only terms passing a given predicate
; sum primes in the interval <a, b]
; sum product of all positive integers less than n
;  that are relatively prime to n, i.e.,
;  sum of positive integers i < n such that GCD(i, n) = 1


(define (filter
         predicate combiner null-value term next a b)
  (if (> a b)
      null-value
      (if (prime? a)
          (combiner (term a)
                (filter predicate
                        combiner null-value
                        term next
                        (next a) b))
          (filter predicate
                  combiner null-value
                  term next
                  (next a) b))))
; Because prime? considers 1 as prime
;  the function gives the correct answer +1
(define (sum-primes n)
  (filter prime?
          + 0
          (lambda (x) x)
          (lambda (x) (+ x 1))
          0 n))

(define (

; BOILERPLATE
(define (prime? n)
  (= n (smallest-divisor n)))

(define (smallest-divisor n)
  (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) 
         n)
        ((divides? test-divisor n) 
         test-divisor)
        (else (find-divisor 
               n 
               (+ test-divisor 1)))))

(define (square x)
  (* x x))

(define (divides? a b)
  (= (remainder b a) 0))

(sum-primes 100)