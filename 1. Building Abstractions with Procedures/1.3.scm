; Exercise 1.3:
; Define a procedure that takes three numbers as arguments
; and returns the sum of the squares of the two larger numbers. 

(define (sqsumlargest a b c)
  (cond
    ((and (>= a c) (>= b c)) (sos a b))
    ((and (>= b a) (>= c a)) (sos b c)) 
    ((and (>= a b) (>= c b)) (sos a c))  
  )
)

(define (sos a b)
  (define (square x)
    (* x x))
  (+ (square a) (square b))
)

(sqsumlargest 1 2 3)