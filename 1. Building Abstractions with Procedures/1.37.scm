#lang racket
(require racket/trace)
; the implementation is actually not good
; variables on terms n & d cannot be controlled

(define (fixed-point-iter f guess k)
  (define next (f guess))
  (if (= k 0)
    next
    (fixed-point-iter f next (- k 1))))


(define (fixed-point f guess k)
  (if (= k 0)
      guess
      (f (fixed-point f guess (- k 1)))))

(define (cont-frac n d k)
  (fixed-point
   (lambda (i) (/ (n i) (+ (d i) i)))
   1
   k))

(trace fixed-point-iter)
(trace fixed-point)
(cont-frac (lambda (i) 1.0)
           (lambda (i) 1.0)
           10)