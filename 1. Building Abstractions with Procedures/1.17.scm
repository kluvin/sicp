(define (* a b)
    (cond ((= b 0) 0)
        ((even? b)
            (double (* a (/ b 2)))
        )
        (else
            (+ a (* a (- b 1)))
        )
    )
)

(define (double x) (+ x x))
(define (halve x) (/ x 2))
(define (even? x) (= (mod x 2) 0))

(* 10 10)