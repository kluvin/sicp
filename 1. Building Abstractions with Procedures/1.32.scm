#lang sicp

; accumulate
;   abstract the type of the operation done in a summation-type pattern


(define (accumulate combiner null-value term next a b)
  (if (> a b)
      null-value
      (combiner (term a)
         (accumulate combiner null-value
                term next
                (next a) b))))

(define (accumulate-iter combiner null-value term next a b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (combiner (term a) result))))
  (iter a null-value))