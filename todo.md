# Chapter 1

* 1.33.2
    * Skipped: Product of positive integers < n relatively prime to n

* 1.37-39
    * Substitute Goal: Expressing continous fractions in terms of a fixed point.
    * Mishap: Implemented continous fraction in terms of fixed point. Since the recursion happens in fixed-point, it is not possible to insert i into *n*, *d*.
    * Reason for skipping: Found correct patterns, need to rewrite cont-frac, which is easy. More interesting problem is expressing continous fractions in terms of a fixed point.
    * Root cause for skipping: Did not read exercise properly.

# Chapter 2
* 2.3
    * Implement perimeter and area calculation of two unique representations of rectangles
    * Dropped because I am not in the mood to implement geometry from scratch.
    * It's also a compromise of how complex you make it. Better go back and do it properly when I have the tools. Making shapes is a delicate business!

* 2.18, 2.20
    * looks ugly

* 2.42-43
  * After spending 12 hours in a single day and finding the flaw. I've permuted the original, nearly working solution so much, I'd rather start over again another time. I think actually having some sort of system to view the structure of lists and pairs, would be really helpful. Interpreting behavior is a major part of debugging, and that just isn't very easy. 

* 2.62
  * Can't figure it out. But rewrote Bill The Lizard's solution, and tried to make the symmetries of the solution more obvious. That aided some in understanding the process.